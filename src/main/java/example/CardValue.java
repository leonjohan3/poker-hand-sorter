package example;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class CardValue {
    public CardValue(final char type) {

        switch (type) {
        case 'A':
            value = 14;
            break;
        case 'K':
            value = 13;
            break;
        case 'Q':
            value = 12;
            break;
        case 'J':
            value = 11;
            break;
        case 'T':
            value = 10;
            break;
        default:
            final char[] tmpType = new char[1];
            tmpType[0] = type;
            value = Integer.parseInt(new String(tmpType));

            if (value < 2) {
                throw new IllegalArgumentException("invalid card value: " + type);
            }
        }
    }

    private int value;
}
