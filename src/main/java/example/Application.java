package example;

import java.util.Scanner;

public class Application {

    public static final int HAND_SIZE = 5;

    public static void main(final String[] args) {
        int playerOneWinningHands = 0;
        int playerTwoWinningHands = 0;

        try (Scanner scanner = new Scanner(System.in)) {

            while (scanner.hasNextLine()) {

                final String[] valueAndSuitsForBothPlayers = scanner.nextLine().split("\\s");

                if (HAND_SIZE * 2 != valueAndSuitsForBothPlayers.length) {
                    throw new IllegalArgumentException("invalid number of cards: " + valueAndSuitsForBothPlayers.length);
                }
                final String[] valuesAndSuitsPlayerOne = new String[HAND_SIZE];
                final String[] valuesAndSuitsPlayerTwo = new String[HAND_SIZE];

                int counter = 0;
                int i = 0;
                int j = 0;

                for (final String valueAndSuit : valueAndSuitsForBothPlayers) {
                    counter++;
                    if (counter <= 5) {
                        valuesAndSuitsPlayerOne[i] = valueAndSuit;
                        i++;
                    } else {
                        valuesAndSuitsPlayerTwo[j] = valueAndSuit;
                        j++;
                    }
                }
                final int winner = PokerWinnerSelector.selectWinner(valuesAndSuitsPlayerOne, valuesAndSuitsPlayerTwo);

                if (1 == winner) {
                    playerOneWinningHands++;
                } else {
                    playerTwoWinningHands++;
                }
            }
        }
        System.out.println("Player 1: " + playerOneWinningHands + " hands");
        System.out.println("Player 2: " + playerTwoWinningHands + " hands");
    }
}
