package example;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class PokerWinnerSelector {

    public static int selectWinner(final String[] valueAndSuitsForPlayerOne, final String[] valueAndSuitsForPlayerTwo) {
        final Set<String> cards = new HashSet<>(Arrays.asList(valueAndSuitsForPlayerOne));
        cards.addAll(Arrays.asList(valueAndSuitsForPlayerTwo));

        if (valueAndSuitsForPlayerOne.length + valueAndSuitsForPlayerTwo.length != cards.size()) {
            throw new IllegalArgumentException("cards dealt are not unique: " + Arrays.asList(valueAndSuitsForPlayerOne) + " and " + Arrays.asList(valueAndSuitsForPlayerTwo));
        }

        final PokerHand playerOneHand = new PokerHand(valueAndSuitsForPlayerOne);
        final PokerHand playerTwoHand = new PokerHand(valueAndSuitsForPlayerTwo);
        final int handPlayerOneRank = playerOneHand.calculateRank();
        final int handPlayerTwoRank = playerTwoHand.calculateRank();

        if (handPlayerOneRank == handPlayerTwoRank) {

            if (playerOneHand.getProminentCardValue() == playerTwoHand.getProminentCardValue()) {
                int winner = 0;
                final SortedSet<Card> nonProminentCardsForOne = new TreeSet<>();

                for (final Card card : playerOneHand.getCards()) {
                    if (playerOneHand.getProminentCardValue() != card.getCardValue().getValue()) {
                        nonProminentCardsForOne.add(card);
                    }
                }
                final SortedSet<Card> nonProminentCardsForTwo = new TreeSet<>();

                for (final Card card : playerTwoHand.getCards()) {
                    if (playerTwoHand.getProminentCardValue() != card.getCardValue().getValue()) {
                        nonProminentCardsForTwo.add(card);
                    }
                }
                int loopCounter = 0;

                while (0 == winner) {

                    if (nonProminentCardsForOne.last().getCardValue().getValue() == nonProminentCardsForTwo.last().getCardValue().getValue()) {
                        nonProminentCardsForOne.remove(nonProminentCardsForOne.last());
                        nonProminentCardsForTwo.remove(nonProminentCardsForTwo.last());
                    } else {
                        if (nonProminentCardsForOne.last().getCardValue().getValue() > nonProminentCardsForTwo.last().getCardValue().getValue()) {
                            winner = 1;
                        } else {
                            winner = 2;
                        }
                    }

                    if (loopCounter++ > Application.HAND_SIZE) {
                        throw new IllegalStateException("error, endless loop encountered");
                    }
                }
                return winner;

            } else {

                if (playerOneHand.getProminentCardValue() > playerTwoHand.getProminentCardValue()) {
                    return 1;
                } else {
                    return 2;
                }
            }
        } else {

            if (handPlayerOneRank > handPlayerTwoRank) {
                return 1;
            } else {
                return 2;
            }
        }
    }
}