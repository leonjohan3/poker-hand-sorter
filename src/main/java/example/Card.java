package example;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class Card implements Comparable<Card> {

    CardValue cardValue;
    Suit suit;

    public Card(final String valueAndSuitAsString) {
        final char[] valueAndSuitAsArray = valueAndSuitAsString.toCharArray();

        if (2 != valueAndSuitAsArray.length) {
            throw new IllegalArgumentException("invalid card: " + valueAndSuitAsString);
        }
        cardValue = new CardValue(valueAndSuitAsArray[0]);
        suit = Suit.of(valueAndSuitAsArray[1]);
    }

    @Override
    public int compareTo(final Card other) {
        final int result = Integer.compare(cardValue.getValue(), other.getCardValue().getValue());

        if (0 != result) {
            return result;
        }
        return suit.compareTo(other.suit);
    }
}
