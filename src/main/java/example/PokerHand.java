package example;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

import lombok.Getter;

@Getter
public class PokerHand {

    private static final int ROYAL_FLUSH_VALUE = 10 + 11 + 12 + 13 + 14;

    private final SortedSet<Card> cards = new TreeSet<>();
    private int prominentCardValue;

    public PokerHand(final String[] someCards) {
        if (Application.HAND_SIZE != someCards.length) {
            throw new IllegalArgumentException("invalid number of cards: " + someCards.length + ", should be " + Application.HAND_SIZE);
        }
        for (final String valueAndSuitAsString : someCards) {
            cards.add(new Card(valueAndSuitAsString));
        }
        if (Application.HAND_SIZE != cards.size()) {
            throw new IllegalArgumentException("invalid cards, not unique: " + Arrays.asList(someCards));
        }
    }

    private int getTotalValueOfAllCards() {
        int total = 0;

        for (final Card card : cards) {
            total += card.getCardValue().getValue();
        }
        return total;
    }

    public int calculateRank() {

        final boolean isAllCardsInSameSuit = isAllCardsInSameSuit();
        boolean isInConsecutiveValueOrder = true;
        int previousCardValue = cards.first().getCardValue().getValue() - 1;
        prominentCardValue = cards.last().getCardValue().getValue();

        for (final Card card : cards) {
            if (card.getCardValue().getValue() != previousCardValue + 1) {
                isInConsecutiveValueOrder = false;
                break;
            }
            previousCardValue = card.getCardValue().getValue();
        }

        if (isAllCardsInSameSuit) {
            final int total = getTotalValueOfAllCards();

            if (ROYAL_FLUSH_VALUE == total) {
                return 10;
            }

            if (isInConsecutiveValueOrder) {
                return 9;
            }
        }

        final Map<Integer, Integer> cardCountByValue = new HashMap<>();

        for (final Card card : cards) {
            if (Objects.isNull(cardCountByValue.get(card.getCardValue().getValue()))) {
                cardCountByValue.put(card.getCardValue().getValue(), 0);
            }
            cardCountByValue.put(card.getCardValue().getValue(), cardCountByValue.get(card.getCardValue().getValue()) + 1);
        }

        for (final Integer cardValue : cardCountByValue.keySet()) {
            final Integer count = cardCountByValue.get(cardValue);

            if (4 == count) {
                prominentCardValue = cardValue;
                return 8;
            }
        }

        for (final Integer cardValue : cardCountByValue.keySet()) {
            final Integer count = cardCountByValue.get(cardValue);

            if (3 == count && 2 == cardCountByValue.size()) {
                prominentCardValue = cardValue;
                return 7;
            }
        }

        if (isAllCardsInSameSuit) {
            return 6;
        }

        if (isInConsecutiveValueOrder) {
            return 5;
        }

        for (final Integer cardValue : cardCountByValue.keySet()) {
            final Integer count = cardCountByValue.get(cardValue);

            if (3 == count) {
                prominentCardValue = cardValue;
                return 4;
            }
        }

        int result = -1;

        for (final Integer cardValue : cardCountByValue.keySet()) {
            final Integer count = cardCountByValue.get(cardValue);

            if (2 == count && 3 == cardCountByValue.size()) {
                prominentCardValue = Math.max(prominentCardValue, cardValue);
                result = 3;
            }
        }

        if (-1 != result) {
            return result;
        }

        for (final Integer cardValue : cardCountByValue.keySet()) {
            final Integer count = cardCountByValue.get(cardValue);

            if (2 == count && 4 == cardCountByValue.size()) {
                prominentCardValue = cardValue;
                return 2;
            }
        }

        return 1;
    }

    private boolean isAllCardsInSameSuit() {
        Suit suit = null;

        for (final Card card : cards) {
            if (null == suit) {
                suit = card.getSuit();
            } else {
                if (!card.getSuit().equals(suit)) {
                    return false;
                }
            }
        }
        return true;
    }
}
