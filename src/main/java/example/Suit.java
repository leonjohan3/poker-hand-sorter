package example;

public enum Suit {
    DIAMONDS, HEARTS, SPADES, CLUBS;

    public static Suit of(final char value) {
        switch (value) {
        case 'D':
            return Suit.DIAMONDS;
        case 'H':
            return Suit.HEARTS;
        case 'S':
            return Suit.SPADES;
        case 'C':
            return Suit.CLUBS;
        default:
            throw new IllegalArgumentException("invalid suit value: " + value);
        }
    };
}
