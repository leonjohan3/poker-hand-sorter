package example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class PokerWinnerSelectorTest {

    @Parameters
    public static Collection<Object[]> testSets() {
        return Arrays.asList(new Object[][] {
            { new String[] { "4H", "4C", "6S", "7S", "KD" }, new String[] { "2C", "3S", "9S", "9D", "TD" }, 2 },
            { new String[] { "5D", "8C", "9S", "JS", "AC" }, new String[] { "2C", "5C", "7D", "8S", "QH" }, 1 },
            { new String[] { "2D", "9C", "AS", "AH", "AC" }, new String[] { "3D", "6D", "7D", "TD", "QD" }, 2 },
            { new String[] { "4D", "6S", "9H", "QH", "QC" }, new String[] { "3D", "6D", "7H", "QD", "QS" }, 1 },
            { new String[] { "2H", "2D", "4C", "4D", "4S" }, new String[] { "3C", "3D", "3S", "9S", "9D" }, 1 },
            { new String[] { "2H", "2D", "4C", "JD", "JS" }, new String[] { "JC", "JH", "3S", "9S", "9D" }, 2 },
        });
    }

    @Parameter(0)
    public String[] handForPlayerOne;

    @Parameter(1)
    public String[] handForPlayerTwo;

    @Parameter(2)
    public int expectedWinner;

    @Test
    public void shouldCalculateWinnerCorrectly() {
        assertThat(PokerWinnerSelector.selectWinner(handForPlayerOne, handForPlayerTwo), is(expectedWinner));
    }
}
