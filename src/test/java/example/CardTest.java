package example;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CardTest {

    private final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Test
    public void shouldThrowExceptionForInvalidCardA() {
        // then : checks and assertions
        getExpectedExceptionRule().expect(IllegalArgumentException.class);
        getExpectedExceptionRule().expectMessage(allOf(containsString("invalid card"), containsString("4H4")));

        // when : method to be checked invocation
        new Card("4H4");
    }

    @Test
    public void shouldThrowExceptionForInvalidCardB() {
        // then : checks and assertions
        getExpectedExceptionRule().expect(IllegalArgumentException.class);
        getExpectedExceptionRule().expectMessage(allOf(containsString("invalid card"), containsString("H")));

        // when : method to be checked invocation
        new Card("H");
    }

    @Test
    public void shouldThrowExceptionForInvalidCardC() {
        // then : checks and assertions
        getExpectedExceptionRule().expect(NumberFormatException.class);
        getExpectedExceptionRule().expectMessage(allOf(containsString("For input string"), containsString("H")));

        // when : method to be checked invocation
        new Card("H4");
    }

    @Test
    public void shouldThrowExceptionForInvalidCardD() {
        // then : checks and assertions
        getExpectedExceptionRule().expect(IllegalArgumentException.class);
        getExpectedExceptionRule().expectMessage(allOf(containsString("invalid card"), containsString("")));

        // when : method to be checked invocation
        new Card("");
    }

    @Test
    public void shouldThrowExceptionForInvalidCardE() {
        // then : checks and assertions
        getExpectedExceptionRule().expect(NullPointerException.class);

        // when : method to be checked invocation
        new Card(null);
    }

    @Test
    public void shouldInstantiateFourOfHearts() {
        final Card card = new Card("4H");
        assertThat(card.getCardValue().getValue(), is(4));
        assertThat(card.getSuit(), is(Suit.HEARTS));
    }
}