package example;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CardValueErrorTest {

    private final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Test
    public void shouldThrowExceptionForInvalidNumber() {
        // then : checks and assertions
        getExpectedExceptionRule().expect(IllegalArgumentException.class);
        getExpectedExceptionRule().expectMessage(allOf(containsString("invalid card value"), containsString("1")));

        // when : method to be checked invocation
        new CardValue('1');
    }

    @Test
    public void shouldThrowExceptionForInvalidNumberFormat() {

        // then : checks and assertions
        getExpectedExceptionRule().expect(NumberFormatException.class);
        getExpectedExceptionRule().expectMessage(allOf(containsString("For input string"), containsString("S")));

        // when : method to be checked invocation
        new CardValue('S');
    }
}