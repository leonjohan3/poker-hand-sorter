package example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CardValueTest {

    @Parameters
    public static Collection<Object[]> testSets() {
        return Arrays.asList(new Object[][] {
            { '2', 2 },
            { '3', 3 },
            { '4', 4 },
            { '5', 5 },
            { '6', 6 },
            { '7', 7 },
            { '8', 8 },
            { '9', 9 },
            { 'T', 10 },
            { 'J', 11 },
            { 'Q', 12 },
            { 'K', 13 },
            { 'A', 14 },
        });
    }

    @Parameter(0)
    public char cardType;

    @Parameter(1)
    public int expectedCardValue;

    @Test
    public void shouldCalculateCardValueCorrectly() {
        final CardValue cardValue = new CardValue(cardType);
        assertThat(cardValue.getValue(), is(expectedCardValue));
    }
}