package example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class PokerHandTest {

    @Parameters
    public static Collection<Object[]> testSets() {
        return Arrays.asList(new Object[][] {
            { new String[] { "TD", "JD", "QD", "KD", "AD" }, 10, 14 },
            { new String[] { "2D", "3D", "4D", "5D", "6D" }, 9, 6   },
            { new String[] { "9H", "TH", "JH", "QH", "KH" }, 9, 13  },
            { new String[] { "QH", "KH", "9H", "TH", "JH" }, 9, 13  },
            { new String[] { "9H", "TH", "9D", "9S", "9C" }, 8, 9   },
            { new String[] { "9H", "4H", "9D", "4S", "9C" }, 7, 9   },
            { new String[] { "2D", "4D", "6D", "AD", "8D" }, 6, 14  },
            { new String[] { "2D", "3H", "4C", "5S", "6D" }, 5, 6   },
            { new String[] { "9H", "KH", "9D", "4S", "9C" }, 4, 9   },
            { new String[] { "9H", "KH", "9D", "4S", "KC" }, 3, 13  },
            { new String[] { "9H", "KH", "6D", "4S", "KC" }, 2, 13  },
            { new String[] { "AD", "2H", "3H", "4D", "5D" }, 1, 14  },
            { new String[] { "9D", "JD", "QD", "5D", "3S" }, 1, 12  },
        });
    }

    @Parameter(0)
    public String[] valuesAndSuits;

    @Parameter(1)
    public int expectedRank;

    @Parameter(2)
    public int expectedProminentCardValue;

    @Test
    public void shouldCalculatePokerHandRankCorrectly() {
        final PokerHand pokerHand = new PokerHand(valuesAndSuits);
        assertThat(pokerHand.calculateRank(), is(expectedRank));
        assertThat(pokerHand.getProminentCardValue(), is(expectedProminentCardValue));
    }
}
