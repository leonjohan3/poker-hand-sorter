## Instructions
1.  Clone the repository and cd into the project folder. `git clone https://leonjohan3@bitbucket.org/leonjohan3/poker-hand-sorter.git`
2.  On a UNIX/Linux type system run `mvn package && java -jar target/poker-hands-sorter-1.0.0.jar < poker-hands.txt`

## Notes
1.  A [Lean software development](https://en.wikipedia.org/wiki/Lean_software_development) approach was followed.
2.  Because of point 1 above, the code was left uncommented and self-commenting names were uses for classes, methods and variables.
3.  The design was simplified to support only the immediate needs in terms of functionality.
4.  Test coverage is not 100% but is probably adequate for future enhancements and refactoring.
5.  This application was tested and provided the correct results on a MacBook Pro, using maven 3.5.3 with Java 1.8